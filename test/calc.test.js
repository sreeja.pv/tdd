const expect = require('chai').expect;
const {calc} = require('../src/calc');

describe("calc", () => {
    it("Should return for addition", () => {
        expect(calc(2, 2, "+")).to.equal(4); 
    });
    it("Should return for substraction", () => {
        expect(calc(2, 2, "-")).to.equal(0); 
    });
    it("Should return for division", () => {
        expect(calc(2, 2, "/")).to.equal(1); 
    });
    it("Should return for multiplication", () => {
        expect(calc(0.3, 0.3, "*")).to.equal(0.09); 
    });
    
})