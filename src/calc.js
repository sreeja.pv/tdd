const calc = (a, b, operator) => {
    switch(operator) {
        case '+' : 
            const add = a + b;
            return add;
        break;
        case '-' : 
            const sub = a - b;
            return sub;
        break;
        case '/' : 
            const div = a / b;
            return div;
        break;
        case '*' : 
            const mul = a * b;
            return mul;
        break;
        default : 
        return "Error!";
    }
    
}

exports.calc = calc;