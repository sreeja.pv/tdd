const fizzbuzz = (num) => {
    // return (num % 5 == 0 ? "buzz" : (num % 7 == 0?"fizz": ""));
    if (num == undefined) { return "Error!"; }
    if ((num % 5 == 0) && (num % 7 == 0)) { return "fizzbuzz"; }
    if ((num % 5 !== 0) && (num % 7 !== 0)) { return ""; }
    if (num % 5 == 0) { return "buzz"; }
    if (num % 7 == 0) { return "fizz"; }
    
}

exports.fizzbuzz = fizzbuzz;